# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Chat app聊天室
* Key functions (add/delete)
    1. 公共聊天室
    2. 一對一私訊
    3. load message history
* Other functions (add/delete)
    1. 首頁有一對一私訊聊天室的連結
    2. 公共聊天室有人發言會有提醒
    3. 私訊對話對方發言會有提醒

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Ｙ|
|GitLab Page|5%|Y|
|Database|15%|Ｙ|
|RWD|15%|Ｙ|
|Topic Key Function|15%|Ｙ|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Ｙ|
|Chrome Notification|5%|Ｙ|
|Use CSS Animation|2.5%|Ｙ|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
* Firebase Hosting URL: https://mychat-ac4f0.firebaseapp.com
* Report: https://gitlab.com/105062313/Midterm_Project/blob/master/README.md
* 使用者系統：採用lab6的註冊、登入、google登入的功能。
* 公眾聊天室：以lab6為基礎，並加上暱稱、說話時間等訊息內容。由於是拿來約跑步的聊天室，所以可以自由更改暱稱（如：新竹24歲男、台中18歲高中妹等等），而點擊他人的暱稱能開啟私訊功能，進入一對一聊天室。
* 私人聊天室：在database開一個chat_room，用來存取私人聊天的訊息。由於是私人聊天，必須防止他人進入，所以chat_room裡每筆資料除了存取雙方的聊天內容，還要存取雙方的uid，這樣便可以利用uid驗證是否有權限進入這個私人聊天室。
* 個人首頁：透過ForEach，取得所有包含自己uid的私人聊天室，並列出聊天室的另一位使用者（email帳號），點擊該email即可進入該私人聊天室繼續聊天。
* 時間：上傳時間是用getTime來抓按下submit的時間。
* 通知：通知是在second_count時判斷,如果database裡的貼文的uid或email不是自己,就跳出通知。

## Security Report (Optional)
