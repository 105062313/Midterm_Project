function init(){
    firebase.auth().onAuthStateChanged(function (user) {
        var information = document.getElementById('information');
        var menu = document.getElementById('dynamic-menu');
        var change_information = document.getElementById('change_information');
        var room1 = document.getElementById('go_room1');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).then(window.location.href = "signin.html").catch(function(e)
                {
                  create_alert("error",e.message);
                });
            })
            room1.addEventListener('click',function(){
                document.location.href = "public.html";
            })

            // database
            var user = firebase.auth().currentUser;
            

            //get chat_list
            var str_before_username = "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
            var str_after_content = "</p></div></div>\n";
            var chat_list = document.getElementById('chat_list');
            chat_list.setAttribute("style","overflow:scroll ; height:356px");

            var chatRef = firebase.database().ref('chat_room');    
            
            var str1="";
            var uid;
            var chat;

            chatRef.once('value').then(function (snapshot) {
                snapshot.forEach(function(childshot){
                    var data = childshot.val();
                    if(data.uid1==user.uid){
                        str1 = str1 + str_before_username + "<span style='font-size:15px' ><a href='sex.html?"  + data.uid2 + "'>" + data.email2 + "</a></span>" + str_after_content;
                        chat_list.innerHTML = str1;
                    }
                    else if(data.uid2==user.uid){
                        str1 = str1 + str_before_username + "<span  style='font-size:15px'><a href='sex.html?"  + data.uid2 + "'>" + data.email1 + "</a></span>" + str_after_content +  "</a>";
                        chat_list.innerHTML = str1;
                    }
                //total_post[total_post.length] =str_before_username + data + str_after_content;
                });
            //history_comment.innerHTML = total_post.join('');
            })
            .catch(e => console.log(e.message));



        } else {
            information.innerHTML = "Please Login";
            change_information.innerHTML = "Login";
            change_information.addEventListener('click',function(){
                window.location.href = "signin.html";
            })
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
            room1.addEventListener('click',function(){
                window.location.href = "signin.html";
            })
        }
    });

}




window.onload = function () {
    init();
};