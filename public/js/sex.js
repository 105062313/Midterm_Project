function init() {
    var menu = document.getElementById('dynamic-menu');
    var user_email = '';
    var host_email='';
    var user_uid;
    var room_uid = location.search;
    var host_uid = room_uid.substring(1,room_uid.strlen);
    var  ref_host= firebase.database().ref('users/' + host_uid);
    ref_host.once("value")
        .then(function(snapshot) {
            host_email = snapshot.val().email;
        });
    if(Notification.permission=== "default"){
        Notification.requestPermission();
    }
    
    firebase.auth().onAuthStateChanged(function (user) {
        
        // Check user login
        if (user) {
            user_email = user.email;
            user_uid=user.uid;
            menu.innerHTML = "<span class='dropdown-item' id='profile-btn'>"+user_email+"</span><span class='dropdown-item' id='home'>Home</span> <span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var home = document.getElementById('home');
            home.addEventListener('click',function(){
                window.location.href = "index.html";
            })
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).catch(function(e)
                {
                  create_alert("error",e.message);
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
        
    });
    var chatRef = firebase.database().ref('chat_room');
    var exist = 0;
    var postsRef;
            
        //connect room
        chatRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childshot){
                        if((user_uid==childshot.val().uid1 || user_uid==childshot.val().uid2) && (host_uid==childshot.val().uid1 || host_uid==childshot.val().uid2)){
                            exist = 1;
                            postsRef = firebase.database().ref('chat_room/' + childshot.key + '/comment');
                            console.log(postsRef);
                            show(postsRef);
                        }
                    })
                    if(exist==0){
                        var data = {
                            uid1:user_uid,
                            uid2:host_uid,
                            email1:user_email,
                            email2:host_email
                        }
                        chatRef.push(data);
                    }
                })
    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
 
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var data = {
                data: post_txt.value,
                email:user_email,
                uid:user_uid,
                time: getTime()
            };
            postsRef.push(data);
            post_txt.value = "";
        }
    });

function show(postsRef){
    var str_before_username ="<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong style='color:black'>";
    var str_after_content = "</p></div></div></div></div>\n";

    
    
    
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    console.log(postsRef);
    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var del_btn = document.getElementsByClassName('btn btn-danger');
            snapshot.forEach(function(childshot){
                var data = childshot.val();
                if(user_email!=data.email){
                    total_post[total_post.length] =str_before_username + data.email+"</strong><span style='padding-left:30px'> " + data.time + "<span></br><span  style='font-size:15px'>" + data.data + "</span>" + str_after_content;
                    first_count += 1
                }
                else{
                    total_post[total_post.length] = str_before_username +'我' + "</strong><span style='padding-left:30px'> " + data.time + "<span></br><span  style='font-size:15px'>" + data.data + "</span>" + str_after_content;
                    first_count += 1
                }
            });

            
            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(user_email!=data.email){
                        total_post[total_post.length] = str_before_username + childData.email+"</strong><span style='padding-left:30px'> " + childData.time + "<span></br><span  style='font-size:15px'>" + childData.data + "</span>" + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                    else{
                        total_post[total_post.length] = str_before_username + '我' + "</strong><span style='padding-left:30px'> " + childData.time + "<span></br><span  style='font-size:15px'>" + childData.data + "</span>" + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                    if(user_email!=childData.email&&Notification.permission==="granted"){
                        var string = childData.email + " 說話ㄌ" 
                        var notification=new Notification(string);
                    }
                    
                }
            });
        })
        .catch(e => console.log(e.message));
    }
}

window.onload = function () {
    init();
};

function getTime() {
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    if (h < 10) {
      h = '0' + h;
    }
    if (m < 10) {
      m = '0' + m;
    }
    if (s < 10) {
      s = '0' + s;
    }
    var now = h + ':' + m + ':' + s;
    return now;
 }
